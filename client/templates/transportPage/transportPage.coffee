Template.transportPage.helpers
  transportId: -> Session.get("transport-id")
  transportName: -> Session.get("transport-name")
  transportData: -> TransportStationsData.find({}, {sort: {secs: 1}})
  loaded: -> Session.get('loaded')

Template.transportPageTransport.rendered = ->
  node = $(@view.firstNode()).find('.wait-time')
  seconds = $(node).attr('seconds')
  $(node).countdown
    until: seconds
    compact: true
    format: 'MS'
    description: ''
    onExpiry: => $(node).text('Прибытие')



loadTransInfoFromCitybus = (id, cb) ->
  $.ajax
    dataType: 'json',
    crossDomain: true,
    url: corsServer + 'http://citybus62.ru/api/prophet/trans_prediction',
    type: 'POST'
    data: {id: id}
  # всегда error. cors такой cors
    success: (data) ->
      parsedResult = data
      result = _.map(parsedResult, (transData) ->
        id: transData.id
        name: transData.name
        secs: parseInt(((moment.utc(transData.dt) - moment().utc()) / 1000).toFixed(), 10)
      )
      cb(null, result)
    error: (xhr, status, text) ->
      cb(status)

Template.transportPage.rendered = ->
  if (!@_rendered)
    @_rendered = true
    $$ = Dom7

    app = window.app
    mainView = window.mainView

    onPageInit = (page) ->
      clearLoadDataIntervalIfExists()

      $('.wait-time').countdown('destroy')
      TransportStationsData.remove({})


      loadData = ->
        console.log('load transport data')
        citybusTransportId = Session.get("transport-id")
        loadTransInfoFromCitybus citybusTransportId, (err, loadedData) ->
          if (err)
            Kadira.trackError("loadTransInfoFromCitybus error", err)
            console.error(err)
            setTimeout(loadData, 1000)
          else
            app.hidePreloader();
            $('.wait-time').countdown('destroy')
            # todo если ничего не получили, то кидать назад?
            TransportStationsData.remove({})
            loadedData.forEach (info) -> TransportStationsData.insert(info)
            if (!Session.get('loaded'))
              setTimeout (-> yaCounter27817242.hit 'transport', "Транспорт #{Session.get("transport-name")}"), 2000
            Session.set('loaded', true)
      app.showPreloader('Загрузка')
      Session.set('loaded', false)
      loadData()
      window.loadDataInterval = setInterval loadData, 30000

    onPageBack = ->
      console.log('transport onPageBack');
      $('.wait-time').countdown('destroy')
      clearLoadDataIntervalIfExists()

    app.onPageInit 'transport', onPageInit
    app.onPageReinit 'transport', onPageInit
    app.onPageBack 'transport', onPageBack