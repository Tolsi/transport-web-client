Template.initPage.rendered = ->
  if (!@_rendered)
    @_rendered = true
    initYandexMetrika = ->
      d = document
      w = window
      c = "yandex_metrika_callbacks"
      (w[c] = w[c] or []).push ->
        try
          w.yaCounter27817242 = new Ya.Metrika(
            id: 27817242
            webvisor: true
            clickmap: true
            trackLinks: true
            accurateTrackBounce: true
            trackHash: true
            params: {}
          )

      n = d.getElementsByTagName("script")[0]
      s = d.createElement("script")
      f = ->
        n.parentNode.insertBefore s, n
      s.type = "text/javascript"
      s.async = true
      s.src = ((if d.location.protocol is "https:" then "https:" else "http:")) + "//mc.yandex.ru/metrika/watch.js"
      if w.opera is "[object Opera]"
        d.addEventListener "DOMContentLoaded", f, false
      else
        f()
        
    initYandexMetrika()

    # init app
    app = new Framework7
      modalTitle: "Транспорт"
      animateNavBackIcon: true
    mainView = app.addView ".view-main",
      dynamicNavbar: true
      domCache: true
      pushState: true
    window.app = app
    window.mainView = mainView