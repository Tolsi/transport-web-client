loadStations = (md5, cb) ->
  $.ajax
    url: corsServer + apiServer + "api/stations",
    type: 'GET'
    data: {md5: md5}
    success: (data, ololo) ->
      cb(null, data)
    error: (xhr, status, text) ->
      cb(status)

Template.stationsListPage.rendered = ->
  if (!@_rendered)
    @_rendered = true
    $$ = Dom7
    
    app = window.app
    mainView = window.mainView

    fillStationsPage = (stations) ->
      app.hidePreloader()
      # Generate array with 10000 demo items:
      items = stations
      # Create virtual list
      virtualList = app.virtualList($$(mainView.container).find(".virtual-list"),
        # Pass array with items
        items: items
      # Custom search function for searchbar
        searchAll: (query, items) ->
          found = []
          i = 0
          regexp = new RegExp(query, "gi")
          while i < items.length
            found.push i  if items[i].name.match(regexp) or query.trim() is ""
            i++
          found #return array with mathced indexes
      # List item Template7 template
        template: "<li>
                          <div class='station' data-id='{{citybusId}}' data-name='{{name}}' data-subname='{{subname}}'><a href='#!?station={{citybusId}}' class='item-link item-content'>
                            <div class='item-inner'>
                              <div class='item-title-row'>
                                <div class='item-title'>{{name}}</div>"+
          #                        <div class='item-after'>200 м</div>
                          "</div>
                          <div class='item-subtitle'>{{subname}}</div>
                          <div class='item-text'></div>
                        </div>
                      </a></div>
                    </li>"
      # Item height
        height: 106
        onItemBeforeInsert: (list, item) ->
          $(item).unbind('click')
          $(item).click (event) ->
            stationEl = $$(item).find('.station')
            Session.set "station",
              citybusId: stationEl.attr("data-id")
              name: stationEl.attr("data-name")
              subname: stationEl.attr("data-subname")
            # todo страницы через iron router
            mainView.router.load pageName: 'station'
      )

      stationIdStr = Router.current().params.query["station"]
      if (stationIdStr)
        stationId = parseInt(stationIdStr, 10)
        station = _.find(stations, (station) -> station.citybusId == stationId)
        if (station)
          Session.set "station", station
          setTimeout (-> mainView.router.load pageName: 'station'), 1000


    stationsPageStore = Store.create({
      name: 'stationsPage',
      version: 1.0
    });
    tryCreateStationsUI = (retry) ->
      if (retry == 0)
        app.showPreloader('Загрузка')
      stationsPageStore.getItem 'stations_md5', (err, lastMd5) ->
        loadStations 1, (err, md5) ->
          if (err)
            Kadira.trackError("loadStations md5 error", err)
            console.error("Error: #{err}")
            if (lastMd5)
              stationsPageStore.getItem 'stations', (stationsText) ->
                stations = JSON.parse(stationsText)
                fillStationsPage(stations)
            else
              # retry
              setTimeout (-> tryCreateStationsUI(retry+1)), 1000
          else if (md5 != lastMd5)
            loadStations 0, (err, loadedStations) ->
              if (err)
                # retry
                console.error("Error: #{err}")
                Kadira.trackError("loadStations error", err)
                setTimeout (-> tryCreateStationsUI(retry+1)), 1000
              else
                stations = loadedStations
                stations = _.filter stations, (station) ->
                  citybusStation =  _.find(station.servicesStations, (servicesStation) -> servicesStation.system == "citybus")
                  if (citybusStation)
                    station.citybusId = citybusStation.id
                  citybusStation
                if (Modernizr.localstorage)
                  stationsPageStore.setItem 'stations_md5', md5, (err) ->
                    if (err)
                      throw err;
                    else
                      stationsPageStore.setItem 'stations', JSON.stringify(stations), (err) ->
                        if (err)
                          throw err;
                        else
                          fillStationsPage(stations)
                else
                  fillStationsPage(stations)
          else
            stationsPageStore.getItem 'stations', (err, stationsText) ->
              if (err)
                throw err;
              else
                clearStorageAndRetry = ->
                  if (Modernizr.localstorage)
                    stationsPageStore.clear (err) ->
                      if (err)
                        throw err;
                      else
                        tryCreateStationsUI(retry+1)
                  else
                    tryCreateStationsUI(retry+1)
                if (stationsText && stationsText != "[]")
                  try
                    stations = JSON.parse(stationsText)
                    fillStationsPage(stations)
                  catch e
                    Kadira.trackError("parse JSON stations from storage error", e)
                    console.error(e)
                    clearStorageAndRetry()
                else
                  clearStorageAndRetry()

    mainView.router.load(pageName: 'stations-list');
    tryCreateStationsUI(0)