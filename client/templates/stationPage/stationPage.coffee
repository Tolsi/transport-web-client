#todo реклама мобильного приложения для Android
#todo информация о разработчике и благодарности м2м
Template.stationPage.helpers
  station: (par) ->
    if (Session.get("station")) then Session.get("station")[par] else null
  loaded: -> Session.get('loaded')
  transportData: -> TransportData.find({}, {sort: {secs: 1}})

Template.stationPageTransport.rendered = ->
  node = $(@view.firstNode()).find('.wait-time')
  seconds = $(node).attr('seconds')
  $(node).countdown
    until: seconds
    compact: true
    format: 'MS'
    description: ''
    onExpiry: => $(node).text('Прибытие')

  transportId = $(@view.firstNode()).find('.item-link').attr('id')
  name = $(@view.firstNode()).find('.item-title').text()
  $(@view.firstNode()).click (event) ->
    Session.set("transport-id", transportId)
    Session.set("transport-name", name)
    mainView.router.load pageName: 'transport'

Template.stationPage.rendered = ->
  if (!@_rendered)
    @_rendered = true
    $$ = Dom7

    app = window.app
    mainView = window.mainView

    loadStationInfoFromCitybus = (id, cb) ->
      $.ajax
        url: corsServer + 'http://citybus62.ru/api/prophet/stops_prediction',
        type: 'POST'
        data: {stops: id}
      # всегда error. cors такой cors
        success: (data) ->
          parsedResult = data
          vehicle_types =
            1: "А"
            2: "Т"
            3: "М"
          result = _.map(parsedResult, (transData) ->
            id: transData.id
            gos: transData.number
            name: vehicle_types[transData.vehicle_type] + transData.name
            secs: parseInt(((moment.utc(transData.dt) - moment().utc()) / 1000).toFixed(), 10)
            type: vehicle_types[transData.vehicle_type]
          )
          cb(null, result)
        error: (xhr, status, text) ->
          console.log("error", data, status)
          cb(status)

    loadStationInfoFromBus62 = (id, cb) ->
      $.ajax
        dataType: 'json',
        crossDomain: true,
        url: corsServer + "http://bus62.ru/php/getStationForecasts.php?sid=#{id}&type=0&city=ryazan&info=01234&_=1419191413757",
      # всегда error. cors такой cors
        success: (data) ->
          parsedResult = data
          result = _.map(parsedResult, (transData) ->
            id: transData.vehid
            gos: transData.number
            name: transData.rtype + transData.rnum
            last: transData.lastst
            where: transData.where
            type: transData.rtype
            secs: parseInt(transData.arrt, 10)
          )
          cb(null, result)
        error: (data, status, xhr) ->
          cb(status)

    onPageInit = (page) ->
      clearLoadDataIntervalIfExists()
        
      $('.wait-time').countdown('destroy')
      TransportData.remove({})

      station = Session.get("station")
      document.title = "#{station.name} (#{station.subname})"

      loadData = ->
        console.log('load station data')
        loadStationInfoFromCitybus station.citybusId, (err, loadedData) ->
          if (err)
            Kadira.trackError("loadStationInfoFromCitybus error", err)
            console.error(err)
            setTimeout(loadData, 1000)
          else
            app.hidePreloader()
            $('.wait-time').countdown('destroy')
            TransportData.remove({})
            loadedData.forEach (info) -> TransportData.insert(info)
            if (!Session.get('loaded'))
              setTimeout (-> yaCounter27817242.hit 'station', "Остановка ##{station.citybusId} #{station.name} (#{station.subname})"), 2000
            Session.set('loaded', true)
      app.showPreloader('Загрузка')
      Session.set('loaded', false)
      loadData()
      window.loadDataInterval = setInterval loadData, 30000

    onPageBack = ->
      console.log('station onPageBack');
      $('.wait-time').countdown('destroy')
      clearLoadDataIntervalIfExists()
      document.title = "Транспорт Рязани"

    app.onPageInit 'station', onPageInit
    app.onPageReinit 'station', onPageInit
    app.onPageBack 'station', onPageBack