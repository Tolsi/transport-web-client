@loadDataInterval = null
@clearLoadDataIntervalIfExists = ->
  if (window.loadDataInterval)
    clearInterval window.loadDataInterval
    window.loadDataInterval = null

# todo env param
@apiServer = "http://t.tolsi.ru:3000/"
@corsServer = "https://cors-anywhere.herokuapp.com/" 