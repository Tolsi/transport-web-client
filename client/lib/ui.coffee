UI.registerHelper 'active', (path) ->
  if (new RegExp("^#{path}$").test(Path.get())) then 'active' else ''

UI.registerHelper 'session', (input) ->
  Session.get(input)

UI.registerHelper 'stringify', (obj) ->
  JSON.stringify(obj)

UI.registerHelper 'length', (obj) ->
  obj.length