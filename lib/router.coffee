Router.configure
  notFoundTemplate: "notFound"

Router.map ->
  @route "app",
    path: '/(.*)'
    template: "app"